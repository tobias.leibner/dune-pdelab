// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:

#ifndef DUNE_PDELAB_INSTATIONARY_ONESTEP_HH
#define DUNE_PDELAB_INSTATIONARY_ONESTEP_HH

#include <dune/pdelab/instationary/explicitonestep.hh>
#include <dune/pdelab/instationary/implicitonestep.hh>

// TODO: Remove include after PDELab 2.4
#include <dune/pdelab/common/instationaryfilenamehelper.hh>

#endif // DUNE_PDELAB_INSTATIONARY_ONESTEP_HH
